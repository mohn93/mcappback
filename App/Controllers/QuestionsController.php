<?php
/**
 * Created by PhpStorm.
 * User: mohn93
 * Date: 6/2/18
 * Time: 1:32 AM
 */

namespace App\Controllers;


use App\Models\Answer;
use App\Models\Question;
use App\Models\WonUsers;
use Slim\Collection;
use Slim\Http\Request;
use Slim\Http\Response;

class QuestionsController
{
    CONST NUMBER_OF_QUESTIONS = 4;

    public function RandomQuestions(Request $request, Response $response)
    {
        $results = Question::query()->with(["Answers"])->get();


        $result = $results->toArray();
        $result1 = Array();
        for ($i = 0; ; $i++) {
            $item = $result[rand(0, $results->count() - 1)];
            if (in_array($item, $result1))
                continue;
            array_push($result1, $item);

            if (count($result1) == QuestionsController::NUMBER_OF_QUESTIONS)
                break;
        }

        $response->getBody()->write(json_encode($result1));

        return $response->withHeader("Content-Type", "application/json");
    }
    public function AllQuestions(Request $request, Response $response)
    {
        $results = Question::query()->with(["Answers"])->get();


        $result = $results->toArray();


        $response->getBody()->write(json_encode($result));

        return $response->withHeader("Content-Type", "application/json");
    }

    public function winner(Request $request, Response $response)
    {

        $name = $request->getParam("name");
        $phoneNumber = $request->getParam("phoneNumber");
        $wonUser = new WonUsers();
        $wonUser->name = $name;
        $wonUser->phone_no = $phoneNumber;
        $wonUser->save();
        $response->getBody()->write("1");
        return $response;
    }

    public function addQuestion(Request $request, Response $response)
    {

        $content = json_decode($request->getBody()->getContents(), true);
        $q = new Question();
        $q->title = $content['title'];
        $q->save();
        foreach ($content['answers'] as $value) {
            $answer = new Answer();
            $answer->question_id = $q->id;
            $answer->answer = $value['answer'];
            $answer->isRight = $value['isRight'];
            $answer->save();
        }
        $response->getBody()->write(1);
        return $response;
    }
    public function deleteQuestion(Request $request, Response $response, $args)
    {
        $id = $args['id'];
        $q = Question::find($id);
        $q->delete();
        $response->getBody()->write(1);
        return $response;
    }
}