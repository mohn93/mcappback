<?php
/**
 * Created by PhpStorm.
 * User: mohn93
 * Date: 6/2/18
 * Time: 1:20 AM
 */


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class WonUsers extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'WonUsers';

}