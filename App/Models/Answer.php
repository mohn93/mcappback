<?php
/**
 * Created by PhpStorm.
 * User: mohn93
 * Date: 1/24/18
 * Time: 11:21 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Answer';


    protected $casts = [
        'isRight' => 'boolean',
    ];
    function Question()
    {
        return $this->belongsTo("App\Models\Question");
    }

}