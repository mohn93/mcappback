<?php
/**
 * Created by PhpStorm.
 * User: mohn93
 * Date: 11/26/17
 * Time: 11:27 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Question extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Question';

    function Answers(){
        return $this->hasMany("App\Models\Answer");
    }

}