<?php
/**
 * Created by PhpStorm.
 * User: mohn93
 * Date: 11/26/17
 * Time: 7:45 PM
 */

namespace App\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use App\Models\User;

class Authentication
{
    protected $excludedPaths;
    protected $container;
    protected $user;

    public function __construct($excludedPaths = [], $container)
    {
        $this->excludedPaths = $excludedPaths;
        $this->container = $container;
    }

    public function __invoke(RequestInterface $request, ResponseInterface $response, $next)
    {
        $auth = $request->getHeader('Token');

        $response = $response->withHeader('Content-type', 'application/json');
        $user = new User();

        $path = $request->getUri()->getPath();

        if (!$this->isInArray($request->getUri()->getPath(), $this->excludedPaths)) {

            //var_dump($auth);
            if (count($auth) == 0) {
                $t = [
                    'status' => "-1", // user is not authorized
                    'res_message' => "Token is not authorized!"
                ];
                $response = $response->withStatus(401);
                $response->withStatus(401)->getBody()->write(json_encode($t));
                return $response;
            } else if (!$user->authenticate($auth)) {
                $t = [
                    'status' => "-1", // user is not authorized
                    'res_message' => "Token is not authorized!"
                ];
               $response = $response->withStatus(401);
                $response->withStatus(401)->getBody()->write(json_encode($t));
                return $response;
            }

        }
        if (count($auth) != 0)
            $user->authenticate($auth);

        $response = $next($request, $response);
        return $response;
    }

    function isInArray($item, $array)
    {
        $isInArray = false;
        foreach ($array as $i) {
            if ("." === $i[strlen($i) - 1]) {
                if (substr($item, 0, strlen($i) - 1) === substr($i, 0, strlen($i) - 1)) {
                    $isInArray = true;
                }
            } else {
                if ($i === $item) {
                    $isInArray = true;
                }
            }

        }
        return $isInArray;
    }
}