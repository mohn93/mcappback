<?php
/**
 * Created by PhpStorm.
 * User: mohn93
 * Date: 11/24/17
 * Time: 10:57 PM
 */



use Respect\Validation\Validator as v;

$validator = v::notEmpty();

$companyValidators = array(
    'name' => $validator,
    'category_id' => $validator,
    'email' => $validator,
    'city_id' => $validator,

);

// Routes

$app->group("/questions", function ()  {
    $this->get("/random", "QuestionsController:RandomQuestions");
    $this->get("", "QuestionsController:AllQuestions");
    $this->post("", "QuestionsController:addQuestion");
    $this->delete("/{id}", "QuestionsController:deleteQuestion");
});
$app->group("/winner", function ()  {
    $this->post("", "QuestionsController:winner");

});





